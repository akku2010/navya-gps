import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';
import { b, r } from '@angular/core/src/render3';
import { OuterSubscriber } from 'rxjs/OuterSubscriber';
import { NgIf } from '@angular/common';
/**
 * Generated class for the WorkingHoursReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-working-hours-report',
  templateUrl: 'working-hours-report.html',
})
export class WorkingHoursReportPage {

  islogin: any;
  Ignitiondevice_id: any = [];
  igiReport: any[] = [];

  datetimeEnd: any;
  datetimeStart: string;
  devices: any;
  isdevice: string;
  portstemp: any;
  datetime: any;
  wrReport = [];
  workingReport: any = [];
  workingReportData: any = [];
  daywiseReport: any = [];
  a: number;
  b: number;
  c: number;
  value: number;
  TotalWorkingHours: number = 0;
  device_id: any = []
  locationEndAddress: any;
  selectedVehicle: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");
  test: any;

  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalligi: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = this.apicalligi.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalligi.startLoading().present();
    this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }
  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.igiReportData);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
    self.file.writeFile(self.file.dataDirectory, "ignition_report_download.xlsx", blob, { replace: true })
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  changeDate(key) {
    this.datetimeStart = undefined;
    if (key === 'today') {
      this.datetimeStart = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeStart = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeStart = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeStart = moment().startOf('month').format();
    }
  }

  getIgnitiondevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    // this.Ignitiondevice_id = selectedVehicle.Device_Name;

    this.Ignitiondevice_id = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.Ignitiondevice_id.push(selectedVehicle[t].Device_Name)
        }
      } else {
        this.Ignitiondevice_id.push(selectedVehicle[0].Device_Name)
      }
    } else return;
    console.log("selectedVehicle=> ", this.Ignitiondevice_id)
  }

  getIgnitiondeviceReport() {

    if (this.Ignitiondevice_id.length === 0) {
      this.Ignitiondevice_id = [];

    }
    let that = this;
    this.apicalligi.startLoading().present();

    this.apicalligi.getWorkingHourReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        console.log("igi", data);
        this.apicalligi.stopLoading();
        this.workingReport = data;

                  for(var r in data ){
                        // console.log('foreach',resp[r]);
                        var tempObj = {
                          'vehName' : r,
                          'ON-OFF_time' : this.workingReport[r]['ON-OFF_time']  ? this.millisecondConversion(this.workingReport[r]['ON-OFF_time']) : 0,
                          'OFF-ON_time' : this.workingReport[r]['OFF-ON_time'] ? this.millisecondConversion(this.workingReport[r]['OFF-ON_time']) : 0
                        }
                        this.wrReport.push(tempObj);
                        console.log("push data", this.wrReport);


                  }

        //this.innerFunc(this.workingReport);
        //  else {
        //   let toast = this.toastCtrl.create({
        //     message: 'Report(s) not found for selected Dates/Vehicle.',
        //     duration: 1500,
        //     position: 'bottom'
        //   })
        //   toast.present();
        // }


      }, error => {
        this.apicalligi.stopLoading();
        console.log(error);
      })
  }




  innerFunc(workingReport) {
               var wrReport = [];
                  for(var r in workingReport ){
                        // console.log('foreach',resp[r]);
                        var tempObj = {
                          'vehName' : r,
                          'ontime': this.millisecondConversion(workingReport[r]['ON-OFF_time']),
                          'offtime' :this.millisecondConversion(workingReport[r]['OFF-ON_time'])
                        }
                        this.workingReportData.push(tempObj);

                  }
                  console.log("push data", this.workingReportData);
  }

  // var wrReport = [];
  // for(var r in resp){
  //       // console.log('foreach',resp[r]);
  //       var tempObj = {
  //         'vehName' : r,
  //         'ON-OFF_time': resp[r]['ON-OFF_time'],
  //         'OFF-ON_time' :resp[r]['OFF-ON_time']
  //       }
  //       wrReport.push(tempObj);

  // }

  millisecondConversion(duration) {
    var minutes = Math.floor((duration / (1000 * 60)) % 60);
    var hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
    hours = (hours < 10) ? 0 + hours : hours;
    minutes = (minutes < 10) ? 0 + minutes : minutes;
    return hours + ":" + minutes;
  }




  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicalligi.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }



  timeToMins(time) {
    var b = time.split(':');
    return b[0]*60 + +b[1];
  }

  // Convert minutes to a time in format hh:mm
  // Returned value is in range 00  to 24 hrs
  timeFromMins(mins) {
    function z(n){return (n<10? '0':'') + n;}
    var h = (mins/60 |0) % 24;
    var m = mins % 60;
    return z(h) + ':' + z(m);
  }

  // Add two times in hh:mm format
    addTimes(a, b) {
    return this.timeFromMins(this.timeToMins(this.a) + this.timeToMins(this.b));
  }

}
